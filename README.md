This project is Lars Mølmann and Ulrik Aasen`s master thesis, automn 2020. 
We are researching different trading strategies we can implement on Golden Ocean Group (GOGL) and BDRY ETF, both securities in the dry bulk sector.
To predict directions for these securites, we are making our main prediction model, which is a Transformer neural network. We also have several benchmarks models and trading strategies, including an ARIMA, VAR and random walk model + a moving average strategy and a buy and hold. 
Our main goal is to find a trading strategy based on price signals from FFA-rates.